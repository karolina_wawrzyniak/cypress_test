
describe('Page header test', () => {
  it('The page header name should be correct', () => {
    cy.visit('https://fred.stlouisfed.org/docs/api/fred/releases.html'); 
    cy.title().should('eq', 'St. Louis Fed Web Services: fred/releases');

  });
});